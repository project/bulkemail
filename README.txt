/**
 * Bulk Mail module for Drupal (bulkmail)
 * Compatible with Drupal 8.x
 *
 * By RajeevChoudhary(https://www.drupal.org/u/rajeevchoudhary) 
 */

CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Bulk Email module provides site administrators an interface to send mass email in easy and quick way.
In order to use the modules like "Views bulk operation" or "views send" module, email addresses should be entities in drupal system but not the such case with this module.
Use case - Lets suppose, you have long list of emails in text file and you want to send email to all email addresses. In that case this module will be helpful.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
For help regarding installation, visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8

CONFIGURATION
-------------

How to use this module
Admin URL - http://yoursites.com/admin/bulk_email
Just copy paste the recipient email addresses in recipient box(one per line) and send emails.


MAINTAINERS
------------

Current Maintainers for Drupal 8 version:
*Rajeev kr. Choudhary (rajeevchoudhary) - https://www.drupal.org/u/rajeevchoudhary


